var names = [
    "Maizie Ellwood",
    "Melody Rivers", 
    "Arlo Ashley"
],
locations = [
    "Menlou'ao",
    "Guadalupe",
    "Dogadda"
    ],
jobs = [
    "Painter", 
    "Chef", 
    "Elementary School Teacher"
];

randomLocations = locations[ Math.floor(Math.random()*locations.length )
],
randomNames = names[ Math.floor(Math.random()*names.length )
],
randomJobs = jobs[ Math.floor(Math.random()*jobs.length )
],
randomInt = Math.floor(Math.random()*10)

var btn = document.getElementById("fortune"); 
btn.onclick = function() {
console.log(
    "You will be a"+ " " + randomJobs,
    "in"+ " " + randomLocations,
    "and married to " + " " + randomNames,
    "with"+ " " + randomInt + " " + "kids.",
);
}
