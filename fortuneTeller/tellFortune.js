    const names = [
        "Maizie Ellwood",
        "Melody Rivers", 
        "Arlo Ashley"
    ],
    locations = [
        "Menlou'ao",
        "Guadalupe",
        "Dogadda"
        ],
    jobs = [
        "Painter", 
        "Chef", 
        "Elementary School Teacher"
    ];

    var btn=document.getElementById("fortune");
    btn.onclick=function tellFortune(randomLocations, randomNames, randomJobs, randomInt) {

        var randomLocations = locations[ Math.floor(Math.random()*locations.length )
        ],
        randomNames = names[ Math.floor(Math.random()*names.length )
        ],
        randomJobs = jobs[ Math.floor(Math.random()*jobs.length )
        ],
        randomInt = Math.floor(Math.random()*10)

        document.getElementById("fortuneKeeper").innerHTML = console.log(
        "You will be a"+ " " + randomJobs,
        "in"+ " " + randomLocations,
        "and married to " + " " + randomNames,
        "with"+ " " + randomInt + " " + "kids.",
        );
    }
